var locationsMock = [{
    'address': 'Calle Mar Adriático, 12, 28221 Majadahonda, Madrid, Spain',
    'variables': {
        'population': 9084,
        'is_reference': true,
        'indexes': {
            'population': 3,
            'unemployment': 1,
            'commercial_activity': 6,
            'wealth': 7,
            'traffic': 4,
            'foreigners': 1,
            'dependency_rate': 2
        }
    }
}, {
    'address': 'Calle Volver a Empezar, 5, 28018, Madrid, Madrid, Spain',
    'variables': {
        'population': 5759,
        'is_reference': false,
        'indexes': {
            'population': 4,
            'unemployment': 7,
            'commercial_activity': 1,
            'wealth': 3,
            'traffic': 2,
            'foreigners': 2,
            'dependency_rate': 6
        }
    }
}, {
    'address': 'Calle de la Virgen de los Peligros, 13, 28410, Manzanares el Real, Madrid, Spain',
    'variables': {
        'population': 1523,
        'is_reference': false,
        'indexes': {
            'population': 5,
            'unemployment': 3,
            'commercial_activity': 7,
            'wealth': 5,
            'traffic': 1,
            'foreigners': 2,
            'dependency_rate': 4
        }
    }
}];

var locationsFormattedMock = [{
    'address': 'Calle Mar Adriático, 12, 28221 Majadahonda, Madrid, Spain',
    'compare': 0,
    'variables': {
        'population': 9084,
        'is_reference': true,
        'indexes': {
            'population': 3,
            'unemployment': 1,
            'commercial_activity': 6,
            'wealth': 7,
            'traffic': 4,
            'foreigners': 1,
            'dependency_rate': 2
        }
    }
}, {
    'address': 'Calle Volver a Empezar, 5, 28018, Madrid, Madrid, Spain',
    'compare': 1,
    'variables': {
        'population': 5759,
        'is_reference': false,
        'indexes': {
            'population': 4,
            'unemployment': 7,
            'commercial_activity': 1,
            'wealth': 3,
            'traffic': 2,
            'foreigners': 2,
            'dependency_rate': 6
        }
    }
}, {
    'address': 'Calle de la Virgen de los Peligros, 13, 28410, Manzanares el Real, Madrid, Spain',
    'compare': 2,
    'variables': {
        'population': 1523,
        'is_reference': false,
        'indexes': {
            'population': 5,
            'unemployment': 3,
            'commercial_activity': 7,
            'wealth': 5,
            'traffic': 1,
            'foreigners': 2,
            'dependency_rate': 4
        }
    }
}];

var chartData = {
    "labels": ["Población", "Desempleo", "Avtividad comercial", "Riqueza", "Trafico peatonal", "Extranjeros", "Tasa de dependencia"],
    "pointRadius": 0,
    "datasets": [{
        "backgroundColor": "rgba(201, 48, 44, .5)",
        "pointBackgroundColor": "rgb(201, 48, 44)",
        "borderColor": "rgb(201, 48, 44)",
        "data": [3, 1, 6, 7, 4, 1, 2],
        "label": "Reference area"
    }, {
        "backgroundColor": "rgba(236, 151, 31, .5)",
        "pointBackgroundColor": "rgb(236, 151, 31)",
        "borderColor": "rgb(236, 151, 31)",
        "data": [4, 7, 1, 3, 2, 2, 6],
        "label": "Compared area 1",
        "fill": "zero"
    }, {
        "backgroundColor": "rgba(51, 122, 183, .5)",
        "pointBackgroundColor": "rgb(51, 122, 183)",
        "borderColor": "rgb(51, 122, 183)",
        "data": [5, 3, 7, 5, 1, 2, 4],
        "label": "Compared area 2",
        "fill": "zero"
    }]
}
