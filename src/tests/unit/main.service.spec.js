/* jshint ignore:start */
describe('mainService', function () {
    //angular services
    var $rootScope, $scope, $httpBackend, $http;
    //app services
    var mainService, environmentConfig, endPointsPaths;
    //variables
    var crtlInject, ctrl, promise, resp, planId, data;

    beforeEach(module('app'));

    beforeEach(angular.mock.inject(function (_$rootScope_, _$httpBackend_, _$http_, _environmentConfig_, _mainService_) {

        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
        $http = _$http_;
        environmentConfig = _environmentConfig_;

        mainService = _mainService_;

        crtlInject = {
            $scope: $scope
        };

        resp = {};
        data = {
            "data": "data to get"
        };

        environmentConfig.apiURL = 'test';

    }));


    /*** component controller definition ***/
    describe("mainService service", function () {
        Then("Should be defined the service", function () {
            expect(mainService).toBeDefined();
        });
    });

    /*** getLocations ***/
    describe("getLocations", function () {
        describe("Call the API end point with the correct parameters", function () {
            beforeEach(function () {
                spyOn($http, "get");
            });

            When(function () {
                mainService.getLocations();
            });

            Then("Should call the correct API end point with the correct paramenters", function () {
                expect($http.get).toHaveBeenCalledWith("test");
            });
        });

        describe("Call getLocationss API end point and return the response", function () {
            Given(function () {
                resp = {};
                $httpBackend.expectGET('test').respond({
                    "data": resp,
                    "status": 200
                });
            });

            When(function () {
                promise = mainService.getLocations();
                $scope.$digest();
                $httpBackend.flush();
            });

            Then("Should return de API response", function () {
                expect(promise.$$state.value.data.data).toEqual(resp);
                expect(promise.$$state.value.data.status).toEqual(200);
                expect(promise.$$state.value.status).toEqual(200);

            });
        });

        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
    });

});
