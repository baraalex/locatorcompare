/* jshint ignore:start */
describe('compareChart', function () {
    //scopes
    var $rootScope, $scope;
    //angular services
    var $httpBackend, $q, $log;
    //app services, factories,...
    var mainService;
    //variables
    var crtlInject, ctrl, def;


    beforeEach(module('app'));

    beforeEach(angular.mock.inject(function ($componentController, _$rootScope_, _$httpBackend_) {

        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;

        crtlInject = {
            $scope: $scope,
        };

        bindings = {
            locations: angular.copy(locationsFormattedMock)
        };

        ctrl = $componentController('compareChart', crtlInject, bindings);

    }));

    /*** component controller definition ***/
    describe("compareChart component", function () {
        Then("Should component controller be defined", function () {
            expect(ctrl).toBeDefined();
        });
    });

    /*** onInit ***/
    describe("$onInit", function () {
        When(function () {
            ctrl.$onInit();
            $scope.$digest();
        });

        Then("Should set data based on location binded", function () {
            expect(ctrl.data).toEqual(chartData);
        });
    });

    /*** showHide ***/
    describe("showHide", function () {
        var index = 0;
        Given(function () {
            ctrl.$onInit();
            $scope.$digest();
            ctrl.data.datasets[index].hidden = false;

            ctrl.chart = {
                update: function () {}
            }
            spyOn(ctrl.chart, 'update');
        })
        When(function () {
            ctrl.showHide(index);
        });

        Then("Should call update chart and set location hidden to hide in chart", function () {
            expect(ctrl.data.datasets[index].hidden).toEqual(true);
            expect(ctrl.chart.update).toHaveBeenCalled();
        });
    });
    /*** download ***/
    describe("download", function () {
        var index = 0;
        Given(function () {
            ctrl.$onInit();
            $scope.$digest();
        })
        When(function () {
            ctrl.download();
        });

        Then("Should set dataDownload and downloadFile to download", function () {
            expect(ctrl.dataDownload).toEqual(JSON.stringify(locationsMock));
            expect(ctrl.downloadFile).toEqual(true);
        });
    });
});
