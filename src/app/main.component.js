(function () {
    angular
        .module('app')
        .component('mainComponent', {
            templateUrl: 'app/main.html',
            controller: mainComponentController,
            controllerAs: '$ctrl'
        });


    function mainComponentController(mainService, $log) {
        var vm = this;
        vm.locations = [];
        vm.error = false;
        vm.loading = true;

        vm.$onInit = function () {
            vm.loading = true;
            mainService.getLocations().then(function (response) {
                vm.locations = response.data;
                setTitles(vm.locations);
            }, function (error) {
                $log.error('ERROR GETTING LOCATIONS COMPARE', error);
                vm.error = true;
            }).finally(function () {
                vm.loading = false;
            });
        };

        /**
         * method to set the title based on if its reference or not
         * 
         * @param {Array} locations - locationst to set teh type
         */
        function setTitles(locations) {
            var compare = 1;
            for (var i = 0; i < locations.length; i++) {
                var location = locations[i];

                location.compare = location.variables.is_reference ? 0 : compare++;

            }
        }

    }
})();
