(function () {
    'use strict';

    angular
        .module('app')
        .service('mainService', mainService);

    /** @ngInject */
    function mainService($http, environmentConfig) {
        var service = {
            getLocations: getLocations
        };

        return service;

        ////////////////

        /**
         * @description method to get locations compare from URL
         * @return {object}      - return a promise object with the api response
         */
        function getLocations() {
            return $http.get(environmentConfig.apiURL);
        }
    }
})();
