(function () {
    'use strict';

    // Usage:
    //  Component to show the information of datasetss to show
    // Bindings
    // - locations : object with locations to show

    angular
        .module('app')
        .component('compareLocationData', {
            templateUrl: 'app/components/compareLocationData/compareLocationData.html',
            controllerAs: '$ctrl',
            bindings: {
                locations: '='
            }
        });
})();
