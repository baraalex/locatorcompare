(function () {
    angular
        .module('app').config(appConfig);

    /** @ngInject */
    function appConfig($stateProvider, $urlRouterProvider, $locationProvider, $logProvider) {
        // locations configuration
        $locationProvider.html5Mode(true).hashPrefix('!');
        $urlRouterProvider.otherwise('/');

        $stateProvider.state('app', {
            url: '/',
            component: 'mainComponent'
        });


        // Turning on console logs
        $logProvider.debugEnabled(true);

    }
})();
