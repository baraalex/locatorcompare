const gulp = require('gulp');

const conf = require('../conf/gulp.conf');

gulp.task('compile-scripts', compileScripts);

function compileScripts() {
    return gulp.src([conf.path.src('**/!(*.html|*.spec|*.mock).js'), '!' + conf.path.src('tests/**')])
        .pipe(gulp.dest(conf.path.tmp()));
}
